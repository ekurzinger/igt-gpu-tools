/*
 * Copyright © 2023 NVIDIA
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "igt.h"
#include "igt_syncobj.h"
#include "sw_sync.h"
#include <unistd.h>
#include <sys/ioctl.h>
#include "drm.h"

/**
 * TEST: syncobj sync file
 * Category: Infrastructure
 * Description: Tests for DRM syncobj sync file import and export
 * Feature: synchronization
 * Functionality: semaphore
 * Run type: FULL
 * Sub-category: DRM
 * Test category: GEM_Legacy
 *
 * SUBTEST: binary-import-export
 * Description: Verifies importing and exporting sync files with a binary syncobj.
 *
 * SUBTEST: timeline-import-export
 * Description: Verifies importing and exporting sync files with a timeline syncobj.
 *
 * SUBTEST: invalid-handle-import
 * Description: Verifies that importing a sync file to an invalid syncobj fails.
 *
 * SUBTEST: invalid-handle-export
 * Description: Verifies that exporting a sync file from an invalid syncobj fails.
 *
 * SUBTEST: invalid-fd-import
 * Description: Verifies that importing an invalid sync file fails.
 *
 * SUBTEST: unsubmitted-export
 * Description: Verifies that exporting a sync file for an unsubmitted point fails.
 *
 */

IGT_TEST_DESCRIPTION("Tests for DRM syncobj sync file import and export");

const char *test_binary_import_export_desc =
	"Verifies importing and exporting a sync file with a binary syncobj.";
static void
test_binary_import_export(int fd)
{
	struct drm_syncobj_sync_file args = { 0 };
	int timeline = sw_sync_timeline_create();

	args.handle = syncobj_create(fd, 0);
	args.fd = sw_sync_timeline_create_fence(timeline, 1);
	args.point = 0;
	igt_assert_eq(__syncobj_import_sync_file(fd, &args), 0);
	close(args.fd);
	args.fd = -1;
	igt_assert_eq(__syncobj_export_sync_file(fd, &args), 0);

	igt_assert(!syncobj_wait(fd, &args.handle, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(args.fd), 0);

	sw_sync_timeline_inc(timeline, 1);
	igt_assert(syncobj_wait(fd, &args.handle, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(args.fd), 1);

	close(args.fd);
	close(timeline);
	syncobj_destroy(fd, args.handle);
}

const char *test_timeline_import_export_desc =
	"Verifies importing and exporting sync files with a timeline syncobj.";
static void
test_timeline_import_export(int fd)
{
	struct drm_syncobj_sync_file args = { 0 };
	int timeline = sw_sync_timeline_create();
	int fence1, fence2;
	uint64_t point1 = 1, point2 = 2;

	args.handle = syncobj_create(fd, 0);
	args.fd = sw_sync_timeline_create_fence(timeline, 1);
	args.point = 1;
	igt_assert_eq(__syncobj_import_sync_file(fd, &args), 0);
	close(args.fd);
	args.fd = -1;
	igt_assert_eq(__syncobj_export_sync_file(fd, &args), 0);
	fence1 = args.fd;

	args.fd = sw_sync_timeline_create_fence(timeline, 2);
	args.point = 2;
	igt_assert_eq(__syncobj_import_sync_file(fd, &args), 0);
	close(args.fd);
	args.fd = -1;
	igt_assert_eq(__syncobj_export_sync_file(fd, &args), 0);
	fence2 = args.fd;

	igt_assert(!syncobj_timeline_wait(fd, &args.handle, &point1, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(fence1), 0);
	igt_assert(!syncobj_timeline_wait(fd, &args.handle, &point2, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(fence2), 0);

	sw_sync_timeline_inc(timeline, 1);
	igt_assert(syncobj_timeline_wait(fd, &args.handle, &point1, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(fence1), 1);
	igt_assert(!syncobj_timeline_wait(fd, &args.handle, &point2, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(fence2), 0);

	sw_sync_timeline_inc(timeline, 1);
	igt_assert(syncobj_timeline_wait(fd, &args.handle, &point1, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(fence1), 1);
	igt_assert(syncobj_timeline_wait(fd, &args.handle, &point2, 1, 0, 0, NULL));
	igt_assert_eq(sync_fence_status(fence2), 1);

	close(fence1);
	close(fence2);
	close(timeline);
	syncobj_destroy(fd, args.handle);
}

const char *test_invalid_handle_import_desc =
	"Verifies that importing a sync file to an invalid syncobj fails.";
static void
test_invalid_handle_import(int fd)
{
	struct drm_syncobj_sync_file args = { 0 };
	int timeline = sw_sync_timeline_create();

	args.handle = 0;
	args.point = 0;
	args.fd = sw_sync_timeline_create_fence(timeline, 1);
	igt_assert_eq(__syncobj_import_sync_file(fd, &args), -EINVAL);

	close(args.fd);
	close(timeline);
}

const char *test_invalid_handle_export_desc =
	"Verifies that exporting a sync file from an invalid syncobj fails.";
static void
test_invalid_handle_export(int fd)
{
	struct drm_syncobj_sync_file args = { 0 };

	args.handle = 0;
	args.point = 0;
	args.fd = -1;
	igt_assert_eq(__syncobj_export_sync_file(fd, &args), -EINVAL);
}

const char *test_invalid_fd_import_desc =
	"Verifies that importing an invalid sync file fails.";
static void
test_invalid_fd_import(int fd)
{
	struct drm_syncobj_sync_file args = { 0 };

	args.handle = syncobj_create(fd, 0);
	args.point = 0;
	args.fd = -1;
	igt_assert_eq(__syncobj_import_sync_file(fd, &args), -EINVAL);

	syncobj_destroy(fd, args.handle);
}

const char *test_unsubmitted_export_desc =
	"Verifies that exporting a sync file for an unsubmitted point fails.";
static void
test_unsubmitted_export(int fd)
{
	struct drm_syncobj_sync_file args = { 0 };

	args.handle = syncobj_create(fd, 0);
	args.point = 0;
	args.fd = -1;
	igt_assert_eq(__syncobj_export_sync_file(fd, &args), -EINVAL);

	syncobj_destroy(fd, args.handle);
}

static bool has_syncobj_timeline(int fd)
{
	uint64_t value;
	return drmGetCap(fd, DRM_CAP_SYNCOBJ_TIMELINE,
			 &value) == 0 && value;
}

static bool has_syncobj_sync_file_import_export(int fd)
{
	struct drm_syncobj_sync_file args = { 0 };
	args.handle = 0xffffffff;
	/* if sync file import/export is supported this will fail with ENOENT,
	 * otherwise it will fail with EINVAL */
	return __syncobj_export_sync_file(fd, &args) == -ENOENT;
}

igt_main
{
	int fd = -1;

	igt_fixture {
		fd = drm_open_driver(DRIVER_ANY);
		igt_require(has_syncobj_timeline(fd));
		igt_require(has_syncobj_sync_file_import_export(fd));
		igt_require_sw_sync();
	}

	igt_describe(test_binary_import_export_desc);
	igt_subtest("binary-import-export")
		test_binary_import_export(fd);

	igt_describe(test_timeline_import_export_desc);
	igt_subtest("timeline-import-export")
		test_timeline_import_export(fd);

	igt_describe(test_invalid_handle_import_desc);
	igt_subtest("invalid-handle-import")
		test_invalid_handle_import(fd);

	igt_describe(test_invalid_handle_export_desc);
	igt_subtest("invalid-handle-export")
		test_invalid_handle_export(fd);

	igt_describe(test_invalid_fd_import_desc);
	igt_subtest("invalid-fd-import")
		test_invalid_fd_import(fd);

	igt_describe(test_unsubmitted_export_desc);
	igt_subtest("unsubmitted-export")
		test_unsubmitted_export(fd);

	igt_fixture {
		drm_close_driver(fd);
	}

}
